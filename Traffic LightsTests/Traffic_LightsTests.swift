//
//  Traffic_LightsTests.swift
//  Traffic LightsTests
//

import XCTest
@testable import Traffic_Lights

class Traffic_LightsTests: XCTestCase {
    
    var intersection: Intersection!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        intersection = Intersection()
        
        let northTrafficLight = TrafficLight(ID: 1, cycleMode: .greenToRed)
        let southTrafficLight = TrafficLight(ID: 2, cycleMode: .greenToRed)
        let eastTrafficLight  = TrafficLight(ID: 3, cycleMode: .greenToRed)
        let westTrafficLight  = TrafficLight(ID: 4, cycleMode: .greenToRed)
        
        intersection.trafficLights = [northTrafficLight, southTrafficLight, eastTrafficLight, westTrafficLight]
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    // This test checks whether traffic lights initialised with the cycle mode .redToRight
    // correctly assign the current traffic light color to red
    func testInitialisedTrafficLightColor() {
        var isColorCorrect: Bool = true
        for trafficLight in intersection.trafficLights {
            if trafficLight.cycleMode == .greenToRed {
                if trafficLight.color != .green {
                    isColorCorrect = false
                    break
                }
            }
            else {
                if trafficLight.color != .red {
                    isColorCorrect = false
                    break
                }
            }
        }
        
        XCTAssertTrue(isColorCorrect, "After traffic light class initialisation an initial color is not setup properly")
    }
    
    // Number of traffic lights after initialisation should be setup properly
    func testIntersectionContainsFourTrafficLightsAfterInitialisation() {
        XCTAssertEqual(intersection.trafficLights.count, 4, "Number of traffic lights at the intersection is different than 4")
    }
    
    
    // Check whether intersection cycle counter updates traffic light counter properly
    func testUpdatedValuesOfATrafficLightsCounterBasedOnIntersectionCycleCounter() {
        intersection.cycleCounter = intersection.cycleTime + 1
        var isCorrect: Bool = true
        for trafficLight in intersection.trafficLights {
                if trafficLight.counter != 1 {
                    isCorrect = false
                }
        }
        XCTAssertTrue(isCorrect, "Setting cycle counter to 31 does not increase the counter of the traffic light")
    }
    
    
    func testCorrectnessOfYellowColorInEachSecondOfASwitchSequence(){
        // Checking sequence of the traffic light for the cycle mode of type .greenToRed
        intersection.cycleCounter = intersection.cycleTime + 2
        var isCorrect: Bool = true
        for trafficLight in intersection.trafficLights {
            if trafficLight.cycleMode == .greenToRed {
                if trafficLight.counter == 2 && trafficLight.color != .amber {
                    isCorrect = false
                    break
                }
            }
        }
        
        XCTAssertTrue(isCorrect, "Value of trafic light color is wrong for this particular counter")
        
        intersection.cycleCounter = intersection.cycleTime + 6
        isCorrect = true
        for trafficLight in intersection.trafficLights {
            if trafficLight.cycleMode == .greenToRed {
                if trafficLight.counter == 6 && trafficLight.color != .amber {
                    isCorrect = false
                    break
                }
            }
        }
        
        XCTAssertTrue(isCorrect, "Value of trafic light color is wrong for counter = 6")
        
        intersection.cycleCounter = intersection.cycleTime + 7
        isCorrect = false
        for trafficLight in intersection.trafficLights {
            if trafficLight.cycleMode == .greenToRed {
                if trafficLight.counter == 7 && trafficLight.color == .amber {
                    isCorrect = true
                    break
                }
            }
        }
        
        XCTAssertFalse(isCorrect, "Current color of traffic light should be different than yellow")
        
        intersection.cycleCounter = intersection.cycleTime + 7
        isCorrect = true
        for trafficLight in intersection.trafficLights {
            if trafficLight.cycleMode == .greenToRed {
                if trafficLight.counter == 7 && trafficLight.color == .red {
                    isCorrect = true
                    break
                }
            }
        }
        
        XCTAssertTrue(isCorrect, "Current color of traffic light should be equal to red")
        
        // Checking sequence of the traffic light for the cycle mode of type .redToGreen
        intersection.cycleCounter = intersection.cycleTime + 6
        isCorrect = false
        for trafficLight in intersection.trafficLights {
            if trafficLight.cycleMode == .redToGreen {
                if trafficLight.counter == 6 && trafficLight.color == .amber {
                    isCorrect = true
                    break
                }
            }
        }
        XCTAssertFalse(isCorrect, "Current color of traffic light should be different than yellow")
        
    }
    
    func testCycleTimeReseting() {
        intersection.cycleCounter = intersection.cycleTime + intersection.switchTime
        intersection.changeLights()
        XCTAssertEqual(intersection.cycleCounter, 0, "After finishing cycle and switching lights the cycle counter should equal to zero")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
