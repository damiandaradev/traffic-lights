# README #


### Description of the approach ###

App consists of two main classes - Intersection and TrafficLight. Intersection class updates each traffic light every 30 seconds. 
Additional time has been added at the end of the cycle in order to handle operation of switching lights. 
When the cycle counter reaches 30s, intersection starts to update each of the traffic light based on its cycle mode. 
There exist two possible cycle modes of the traffic lights that help to distinguish which particular sequence should be performed on the traffic lights. For example if the cycle mode is set to .redToGreen, it means that in the current cycle traffic light will be switched from red to green. 
Each traffic light contains its own counter that is only updated during the switch operation. 
Solution strongly relies on a Timer class that updates values of a cycleCounter every second. Pressing on a Start/stop button either start the cycle or pauses the traffic lights in the current state.