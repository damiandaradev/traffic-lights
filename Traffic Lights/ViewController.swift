//
//  ViewController.swift
//  Traffic Lights
//

import UIKit

class ViewController: UIViewController {
    
    var lightsView: LightsView!
    var timer: Timer!
    var trafficLights: [TrafficLight]!
    var queue: DispatchQueue!
    var intersection: Intersection!
    
    override func loadView() {
        super.loadView()
        lightsView = LightsView()
        lightsView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view = lightsView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lightsView.button.addTarget(self, action: #selector(ViewController.buttonAction), for: .touchUpInside)
        
        intersection = Intersection()
        intersection.delegate = self
        
        // Subscribe to each traffic light at the intersection
        // so that the view controller can be notified about the changes
        for trafficLight in intersection.trafficLights {
            trafficLight.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Start/stop button functionality for starting/stoping traffic light cycle
    func buttonAction() {
        
        lightsView.button.isSelected = !lightsView.button.isSelected
        let isSelected = lightsView.button.isSelected
        
        if isSelected {
            intersection.startCycle()
            lightsView.button.setTitle("Stop", for: .normal)
        }
        else {
            intersection.stopCycle()
            lightsView.button.setTitle("Start", for: .normal)
        }
    }
    
    // This function returns an image for the specific light color
    func imageForLightColor(color: LightColor) -> UIImage {
        switch color {
        case .red:
            return #imageLiteral(resourceName: "red")
        case .green:
            return #imageLiteral(resourceName: "green")
        case .amber:
            return #imageLiteral(resourceName: "amber")
        }
    }
    
    // This function updates the color of a traffic light
    func updateTrafficLightWithID(trafficLightID: Int, color: LightColor) {
        switch trafficLightID {
        case 1:
            lightsView.northLights.image = imageForLightColor(color: color)
            break
        case 2:
            lightsView.southLights.image = imageForLightColor(color: color)
            break
        case 3:
            lightsView.eastLights.image = imageForLightColor(color: color)
            break
        case 4:
            lightsView.westLights.image = imageForLightColor(color: color)
            break
        default:
            break
        }
    }
    
}

extension ViewController: TrafficLightDelegate {
    // Update user interface with the proper light color
    func didSwitchLight(trafficLightID: Int, to color: LightColor) {
        updateTrafficLightWithID(trafficLightID: trafficLightID, color: color)
    }
}

extension ViewController: IntersectionDelegate {
    // Update user interface with the current value of the cycleCounter
    func updateCounter(value: Int) {
        lightsView.timerLabel.text = value > 30 ? "Switch time: \(value-30)" : "\(value)"
    }
}

