//
//  LightsView.swift
//  Traffic Lights
//
//  Created by Damian Dara 🇵🇱 on 23/4/17.
//
//

import UIKit

class LightsView: UIView {
    
    var southLights: UIImageView! {
        didSet {
            southLights.translatesAutoresizingMaskIntoConstraints = false
            southLights.image = #imageLiteral(resourceName: "green")
            southLights.contentMode = .scaleAspectFit
        }
    }
    
    var northLights: UIImageView! {
        didSet {
            northLights.translatesAutoresizingMaskIntoConstraints = false
            northLights.image = #imageLiteral(resourceName: "green")
            northLights.contentMode = .scaleAspectFit
            northLights.transform = northLights.transform.rotated(by: CGFloat(Double.pi))
        }
    }
    
    var eastLights: UIImageView! {
        didSet {
            eastLights.translatesAutoresizingMaskIntoConstraints = false
            eastLights.image = #imageLiteral(resourceName: "red")
            eastLights.contentMode = .scaleAspectFit
            eastLights.transform = eastLights.transform.rotated(by: CGFloat(-Double.pi/2))
        }
    }
    
    var westLights: UIImageView! {
        didSet {
            westLights.translatesAutoresizingMaskIntoConstraints = false
            westLights.image = #imageLiteral(resourceName: "red")
            westLights.contentMode = .scaleAspectFit
            westLights.transform = westLights.transform.rotated(by: CGFloat(Double.pi/2))
        }
    }
    
    var button: UIButton! {
        didSet {
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle("Start", for: .normal)
        }
    }
    
    var timerLabel: UILabel! {
        didSet {
            timerLabel.translatesAutoresizingMaskIntoConstraints = false
            timerLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            timerLabel.textAlignment = .right
        }
    }
    
    // Store all layout constraints in an array for compact and regular size classes
    // + store shared constraints that don't need to be updated in the separate set
    private var sharedConstraints: [NSLayoutConstraint]!
    private var compactConstraints: [NSLayoutConstraint]!
    private var regularConstraints: [NSLayoutConstraint]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
        initConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initViews()
        initConstraints()
    }
    
    func initViews() {
        backgroundColor = .white
        
        northLights = UIImageView()
        addSubview(northLights)
        
        southLights = UIImageView()
        addSubview(southLights)
        
        eastLights = UIImageView()
        addSubview(eastLights)
        
        westLights = UIImageView()
        addSubview(westLights)
        
        button = UIButton(type: .roundedRect)
        addSubview(button)
        
        timerLabel = UILabel()
        addSubview(timerLabel)
    }
    
    func initConstraints() {
        // load constraints into an arrays
        sharedConstraints = loadSharedConstraints()
        compactConstraints = loadCompactConstraints()
        regularConstraints = loadRegularConstraints()
        
        // Check initial device orientation to activate right constraints
        let deviceOrientation = UIDevice.current.orientation
        if deviceOrientation == .portrait || deviceOrientation == .portraitUpsideDown {
            NSLayoutConstraint.activate(regularConstraints)
        }
        else {
            NSLayoutConstraint.activate(compactConstraints)
        }
        NSLayoutConstraint.activate(sharedConstraints)
    }
    
    func loadCompactConstraints() -> [NSLayoutConstraint] {
        // north traffic lights constraints for the compact size class
        let northLightsCenterX = northLights.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 100)
        northLightsCenterX.identifier = "northLightsCenterX"
        let northLightsTop = northLights.topAnchor.constraint(equalTo: self.topAnchor, constant: 50)
        northLightsTop.identifier = "northLightsTop"
        let northLightsHeight = northLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        northLightsHeight.identifier = "northLightsHeight"
        let northLightsWidth = northLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.05)
        northLightsWidth.identifier = "northLightsWidth"
        
        // south traffic lights constraints for the compact size class
        let southLightsCenterX = southLights.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: -100)
        southLightsCenterX.identifier = "southLightsCenterX"
        let southLightsBottom = southLights.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -50)
        southLightsBottom.identifier = "southLightsBottom"
        let southLightsHeight = southLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        southLightsHeight.identifier = "southLightsHeight"
        let southLightsWidth = southLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.05)
        southLightsWidth.identifier = "southLightsWidth"
        
        // east traffic lights constraints for the compact size class
        let eastLightsCenterY = eastLights.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 50)
        eastLightsCenterY.identifier = "eastLightsCenterY"
        let eastLightsLeft = eastLights.leftAnchor.constraint(equalTo: northLights.leftAnchor, constant: 0)
        eastLightsLeft.identifier = "eastLightsLeft"
        let eastLightsHeight = eastLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        eastLightsHeight.identifier = "eastLightsHeight"
        let eastLightsWidth = eastLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.05)
        eastLightsWidth.identifier = "eastLightsWidth"
        
        // west traffic lights constraints for the compact size class
        let westLightsCenterY = westLights.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -50)
        westLightsCenterY.identifier = "westLightsCenterY"
        let westLightsRight = westLights.rightAnchor.constraint(equalTo: southLights.rightAnchor, constant: 0)
        westLightsRight.identifier = "westLightsRight"
        let westLightsHeight = westLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        westLightsHeight.identifier = "westLightsHeight"
        let westLightsWidth = westLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.05)
        westLightsWidth.identifier = "westLightsWidth"

        return [
            northLightsWidth, northLightsHeight, northLightsCenterX, northLightsTop,
            southLightsWidth, southLightsHeight, southLightsCenterX, southLightsBottom,
            eastLightsWidth, eastLightsHeight, eastLightsCenterY, eastLightsLeft,
            westLightsWidth, westLightsHeight, westLightsCenterY, westLightsRight
        ]
    }
    
    func loadRegularConstraints() -> [NSLayoutConstraint] {
        // north traffic lights constraints for the regular size class
        let northLightsCenterX = northLights.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 100)
        northLightsCenterX.identifier = "northLightsCenterX"
        let northLightsTop = northLights.topAnchor.constraint(equalTo: self.topAnchor, constant: 50)
        northLightsTop.identifier = "northLightsTop"
        let northLightsHeight = northLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.25)
        northLightsHeight.identifier = "northLightsHeight"
        let northLightsWidth = northLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1)
        northLightsWidth.identifier = "northLightsWidth"
        
        // south traffic lights constraints for the regular size class
        let southLightsCenterX = southLights.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: -100)
        southLightsCenterX.identifier = "southLightsCenterX"
        let southLightsBottom = southLights.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -50)
        southLightsBottom.identifier = "southLightsBottom"
        let southLightsHeight = southLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.25)
        southLightsHeight.identifier = "southLightsHeight"
        let southLightsWidth = southLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1)
        southLightsWidth.identifier = "southLightsWidth"
        
        // east traffic lights constraints for the regular size class
        let eastLightsCenterY = eastLights.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 50)
        eastLightsCenterY.identifier = "eastLightsCenterY"
        let eastLightsRight = eastLights.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -50)
        eastLightsRight.identifier = "eastLightsRight"
        let eastLightsHeight = eastLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.25)
        eastLightsHeight.identifier = "eastLightsHeight"
        let eastLightsWidth = eastLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1)
        eastLightsWidth.identifier = "eastLightsWidth"
        
        // west traffic lights constraints for the regular size class
        let westLightsCenterY = westLights.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -50)
        westLightsCenterY.identifier = "westLightsCenterY"
        let westLightsLeft = westLights.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 50)
        westLightsLeft.identifier = "westLightsLeft"
        let westLightsHeight = westLights.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.25)
        westLightsHeight.identifier = "westLightsHeight"
        let westLightsWidth = westLights.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1)
        westLightsWidth.identifier = "westLightsWidth"
        
        return [
            northLightsWidth, northLightsHeight, northLightsCenterX, northLightsTop,
            southLightsWidth, southLightsHeight, southLightsCenterX, southLightsBottom,
            eastLightsWidth, eastLightsHeight, eastLightsCenterY, eastLightsRight,
            westLightsWidth, westLightsHeight, westLightsCenterY, westLightsLeft
        ]
    }
    
    func loadSharedConstraints() -> [NSLayoutConstraint] {
        // Button constraints
        let buttonBottom = button.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        let buttonCenterX = button.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let buttonHeight = button.heightAnchor.constraint(equalToConstant: 50)
        let buttonWidth = button.widthAnchor.constraint(equalToConstant: 100)
        
        // Timer label constraints
        let timerLabelRight = timerLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15)
        let timerLabelTop = timerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 15)
        let timerLabelWidth = timerLabel.widthAnchor.constraint(equalToConstant: 120)
        let timerLabelHeight = timerLabel.heightAnchor.constraint(equalToConstant: 40)
        
        
        return [
            buttonWidth, buttonHeight, buttonCenterX, buttonBottom,
            timerLabelTop, timerLabelRight, timerLabelWidth, timerLabelHeight
        ]
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if self.traitCollection.containsTraits(in: UITraitCollection(verticalSizeClass: .compact)) {
            NSLayoutConstraint.deactivate(regularConstraints)
            NSLayoutConstraint.activate(compactConstraints)
        }
        
        if self.traitCollection.containsTraits(in: UITraitCollection(verticalSizeClass: .regular)) {
            NSLayoutConstraint.deactivate(compactConstraints)
            NSLayoutConstraint.activate(regularConstraints)
        }

    }
}
