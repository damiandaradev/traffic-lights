//
//  TrafficLight.swift
//  Traffic Lights
//
//  Created by Damian Dara 🇵🇱 on 23/4/17.
//
//

import Foundation

protocol TrafficLightDelegate {
    // Notify subscribers about switching traffic lights to the different color
    func didSwitchLight(trafficLightID: Int, to color: LightColor)
}

class TrafficLight {
    var delegate: TrafficLightDelegate?
    
    // ID helps to distinguish between different traffic lights
    var ID: Int
    // Current color value of the traffic lights
    var color: LightColor
    // Cycle mode - defines state of the light transition
    var cycleMode: CycleMode
    // Counter helps to perform a switch sequance of the traffic light
    var counter: Int = 0
    
    
    init(ID: Int, cycleMode: CycleMode) {
        self.ID = ID
        self.cycleMode = cycleMode
        self.color = cycleMode == .redToGreen ? .red : .green
    }
    
    // This function switches light based to the specified color
    func switchLight(to: LightColor) {
        if let delegate = delegate {
            delegate.didSwitchLight(trafficLightID: ID, to: to)
        }
    }
    
    // Update the value of the counter and based on the state 
    // of a sequence switch the traffic lights to the proper color
    func updateCounter() {
        counter = counter + 1
        if cycleMode == .redToGreen {
            if counter == 6 {
                switchLight(to: .amber)
            }
            else if counter == 7 {
                switchLight(to: .green)
            }
        }
        else {
            if counter == 1 {
                switchLight(to: .amber)
            }
            else if counter == 6 {
                switchLight(to: .red)
            }
        }
        
        // reset counter when the operation of switching lights is finished
        if counter == 7 {
            counter = 0
        }
    }
    
}

enum LightColor {
    case red
    case amber
    case green
}

enum CycleMode {
    case redToGreen
    case greenToRed
}
