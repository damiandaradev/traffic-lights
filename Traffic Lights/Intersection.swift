//
//  Intersection.swift
//  Traffic Lights
//
//  Created by Damian Dara 🇵🇱 on 23/4/17.
//
//

import Foundation


protocol IntersectionDelegate {
    // Notify subscribers about the current cycle counter value
    func updateCounter(value:Int)
}

final class Intersection {
    var delegate: IntersectionDelegate?
    
    // Lights change automatically every 30 seconds
    let cycleTime: Int = 30
    
    // After every cycle there is an additional time for switching the lights
    // 7 = amber display time (5) + time required for switching lights to red and green (1+1)
    let switchTime: Int = 7
    
    // Each intersection contains set of traffic lights
    var trafficLights: [TrafficLight]!
    
    // Timer which updates values of cycle counter
    private var timer: Timer!
    
    // Cycle counter - stores current value of the cycle state
    var cycleCounter: Int = 0 {
        didSet {
            // send updated cycle counter value to the subscribed classes
            if let delegate = delegate {
                delegate.updateCounter(value: cycleCounter)
            }
            
            // Change the lights every cycle time (30s)
            if cycleCounter > cycleTime {
                changeLights()
            }
            
            // After changing the lights reset the cycle counter
            if cycleCounter == cycleTime + switchTime {
                cycleCounter = 0
            }
        }
    }
    
    init() {
        
        // Create and add each traffic light to the intersection
        let northTrafficLight = TrafficLight(ID: 1, cycleMode: .greenToRed)
        let southTrafficLight = TrafficLight(ID: 2, cycleMode: .greenToRed)
        let eastTrafficLight  = TrafficLight(ID: 3, cycleMode: .redToGreen)
        let westTrafficLight  = TrafficLight(ID: 4, cycleMode: .redToGreen)
        
        trafficLights = [northTrafficLight, southTrafficLight, eastTrafficLight, westTrafficLight]
    }
    
    // This function initialise the timer that increases the value of cycleCounter every second
    func startCycle() {
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (t) in
                self.cycleCounter = self.cycleCounter + 1
            })
            RunLoop.current.add(timer, forMode: .commonModes)
        } else {
            // Fallback on earlier versions
        }
    }
    
    // This function stops the timer - cycleCounter's value won't be increased
    func stopCycle() {
        timer.invalidate()
    }
    
    // This function performs an operation of updating counter of each traffic light at the intersection
    func changeLights() {
        for trafficLight in trafficLights {
            trafficLight.updateCounter()
        }
    }
    
    
}
